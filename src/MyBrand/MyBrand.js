import React, { useEffect, useState } from 'react'
import axios from 'axios'


export const MyBrand = (props) => {
    const [cat, setCat] = useState('')
    const [err, setError] = useState(null)
    useEffect(() => {
        async function fetchData() {
            await axios.get('https://social-web-app-duplicate.herokuapp.com/textTocat').then(res => {
                setCat(res.data)
            }).catch(err => {
                setError(err)
            })
        }
        fetchData();

    }, [])
    return (
        <div>
            <h3>My Brand</h3>
            <p>Provide example of publication you have done in past or would have written yourself.</p>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <h5>Category</h5>
                <h5>Post Concept</h5>
                <h5>Caption</h5>
            </div>
            {Object.keys(cat)?.map(key => {
                return (
                    <div style={{ display: 'flex', justifyContent: 'space-between',marginBottom:'20px' }}>
                        {console.log('concept', cat)}
                        <select name="cat" id="categories">
                            {Object.keys(cat)?.map(categories => {
                                return <option>{categories}</option>
                            })}
                        </select>
                        <input type='text' placeholder="Concept" />
                        <input type='text' placeholder="Caption" />
                    </div>
                )
            })}


        </div>
    )
}