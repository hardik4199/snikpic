import './App.css';
import {SideBar} from './sidebar/SideBar'
import {Switch,Route} from 'react-router-dom'
import {MyBrand} from './MyBrand/MyBrand'
import {MyPublication} from './MyPublication/MyPublication'

function App() {
  return (
    <div style={{display:'flex'}}>
        <SideBar />
        <Switch>
          <Route path='/' exact component={MyBrand} />
          <Route path='/mypublication' exact component={MyPublication} />
        </Switch>
    </div>
  );
}

export default App;
