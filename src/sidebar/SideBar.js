import React from 'react'
import './SideBar.css'
import {Link} from 'react-router-dom'

export const SideBar = (props) => {
    return(
        <div className="sidebar">
            <Link to='/' style={{textDecoration:'none'}}>My Brand</Link>
            <Link to='/mypublication' style={{textDecoration:'none'}}>My publication</Link>
        </div>
    )
}